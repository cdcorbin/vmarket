'''
Created on Mar 11, 2016

@author: cdcorbin
'''
import unittest
from pnnl.polyline import PolyLineFactory, PolyLine, Point
from pnnl.market import Market
from pnnl.offer import Offer


class Test(unittest.TestCase):


    def setUp(self):
        pass


    def tearDown(self):
        pass


    def testIntersect1(self):
        demand = [(0.0, 50.0), (0.0, 49.74747474747475), (0.0, 49.494949494949495), (0.0, 49.24242424242424), (0.0, 48.98989898989899), (0.0, 48.737373737373744), (0.0, 48.484848484848484), (0.0, 48.23232323232324), (0.0, 47.97979797979798), (0.0, 47.727272727272734), (0.0, 47.474747474747474), (0.0, 47.22222222222223), (0.0, 46.96969696969697), (0.0, 46.71717171717172), (0.0, 46.464646464646464), (0.0, 46.21212121212122), (0.0, 45.95959595959596), (0.0, 45.70707070707071), (0.0, 45.45454545454545), (0.0, 45.20202020202021), (0.0, 44.94949494949495), (0.0, 44.6969696969697), (0.0, 44.44444444444444), (0.0, 44.1919191919192), (0.0, 43.93939393939394), (0.0, 43.68686868686869), (0.0, 43.43434343434343), (0.0, 43.18181818181819), (0.0, 42.92929292929293), (0.0, 42.67676767676768), (0.0, 42.42424242424242), (0.0, 42.17171717171718), (0.0, 41.91919191919192), (0.0, 41.66666666666667), (0.0, 41.41414141414141), (0.0, 41.161616161616166), (0.0, 40.90909090909091), (0.0, 40.65656565656566), (0.0, 40.4040404040404), (0.0, 40.151515151515156), (0.0, 39.898989898989896), (0.0, 39.64646464646465), (0.0, 39.39393939393939), (0.0, 39.141414141414145), (0.0, 38.888888888888886), (0.0, 38.63636363636364), (0.0, 38.38383838383838), (0.0, 38.131313131313135), (0.0, 37.878787878787875), (0.0, 37.62626262626263), (0.0, 37.37373737373737), (0.0, 37.121212121212125), (0.0, 36.86868686868687), (0.0, 36.61616161616162), (0.0, 36.36363636363637), (0.0, 36.111111111111114), (0.0, 35.85858585858586), (0.0, 35.60606060606061), (0.0, 35.35353535353536), (0.0, 35.101010101010104), (0.0, 34.84848484848485), (0.0, 34.5959595959596), (0.0, 34.343434343434346), (0.0, 34.09090909090909), (0.0, 33.83838383838384), (0.0, 33.58585858585859), (0.0, 33.333333333333336), (0.0, 33.08080808080808), (0.0, 32.82828282828283), (0.0, 32.57575757575758), (0.0, 32.323232323232325), (0.0, 32.07070707070707), (0.0, 31.81818181818182), (0.0, 31.565656565656568), (0.0, 31.313131313131315), (0.0, 31.060606060606062), (0.0, 30.80808080808081), (0.0, 30.555555555555557), (0.0, 30.303030303030305), (0.0, 30.050505050505052), (0.0, 29.7979797979798), (0.0, 29.545454545454547), (0.0, 29.292929292929294), (0.0, 29.04040404040404), (0.0, 28.78787878787879), (0.0, 28.535353535353536), (0.0, 28.282828282828284), (0.0, 28.03030303030303), (0.0, 27.77777777777778), (0.0, 27.525252525252526), (0.0, 27.272727272727273), (0.0, 27.02020202020202), (0.0, 26.767676767676768), (0.0, 26.515151515151516), (0.0, 26.262626262626263), (0.0, 26.01010101010101), (0.0, 25.757575757575758), (0.0, 25.505050505050505), (0.0, 25.252525252525253), (0.0, 25.0)]
        supply = [(0.0, 25.096926), (0.0, 25.096926)]
        dPoly = PolyLineFactory.fromTupples(demand)
        sPoly = PolyLineFactory.fromTupples(supply)
        print PolyLine.intersection(dPoly.tuppleize(), sPoly.tuppleize())    
        sPoly = PolyLine()
        sPoly.add(Point(0.0, 25.096926))
        sPoly.add(Point(0.0, 25.096926))
        print PolyLine.intersection(dPoly.tuppleize(), sPoly.tuppleize())
        
        
    def testIntersect2(self):
        # should settle at $10
        demand = [(0.0, 50.0), (100.0, 10.0)]
        supply = [(0.0, 0.0), (100.0, 10.0)]
        dPoly = PolyLineFactory.fromTupples(demand)
        sPoly = PolyLineFactory.fromTupples(supply)
        print PolyLine.intersection(dPoly.tuppleize(), sPoly.tuppleize())

                    
    def testIntersect3(self):
        # should not clear
        demand = [(0.0, 50.0), (100.0, 10.0)]
        supply = [(0.0, 0.0), (100.0, 9.0)]
        dPoly = PolyLineFactory.fromTupples(demand)
        sPoly = PolyLineFactory.fromTupples(supply)
        mkt= Market()
        mkt.name = "testMarket"
        mkt.commodity = "commodity"
        offers = []
        offers.append(Offer(Offer.BUY, "commodity", dPoly))
        offers.append(Offer(Offer.SELL, "commodity", sPoly))
        mkt.offers = offers
        mkt.settle()
        
        
    def testIntersect4(self):
        # should not clear
        demand = [(0.0, 50.0), (100.0, 10.0)]
        supply = [(0.0, 100.0), (100.0, 200.0)]
        dPoly = PolyLineFactory.fromTupples(demand)
        sPoly = PolyLineFactory.fromTupples(supply)
        mkt= Market()
        mkt.name = "testMarket"
        mkt.commodity = "commodity"
        offers = []
        offers.append(Offer(Offer.BUY, "commodity", dPoly))
        offers.append(Offer(Offer.SELL, "commodity", sPoly))
        mkt.offers = offers
        mkt.settle()
        
        
    def testIntersect5(self):
        # should not clear
        demand = [(0.0, 50.0), (100.0, 10.0)]
        supply = [(0.0, 0.0), (50.0, 5.0)]
        dPoly = PolyLineFactory.fromTupples(demand)
        sPoly = PolyLineFactory.fromTupples(supply)
        mkt= Market()
        mkt.name = "testMarket"
        mkt.commodity = "commodity"
        offers = []
        offers.append(Offer(Offer.BUY, "commodity", dPoly))
        offers.append(Offer(Offer.SELL, "commodity", sPoly))
        mkt.offers = offers
        mkt.settle()
        
        
    def testIntersect6(self):
        # should clear at $50
        demand = [(0.0, 50.0), (100.0, 10.0)]
        supply = [(0.0, 0.0), (50.0, 5.0), (50.0, Offer.HUGE)]
        dPoly = PolyLineFactory.fromTupples(demand)
        sPoly = PolyLineFactory.fromTupples(supply)
        mkt= Market()
        mkt.name = "testMarket"
        mkt.commodity = "commodity"
        offers = []
        offers.append(Offer(Offer.BUY, "commodity", dPoly))
        offers.append(Offer(Offer.SELL, "commodity", sPoly))
        mkt.offers = offers
        mkt.settle()
        
        
    def testIntersect7(self):
        # should clear at $10
        demand = [(0.0, 50.0), (100.0, 10.0)]
        supply = [(100.0, 10.0), (500.0, 50.0)]
        dPoly = PolyLineFactory.fromTupples(demand)
        sPoly = PolyLineFactory.fromTupples(supply)
        mkt= Market()
        mkt.name = "testMarket"
        mkt.commodity = "commodity"
        offers = []
        offers.append(Offer(Offer.BUY, "commodity", dPoly))
        offers.append(Offer(Offer.SELL, "commodity", sPoly))
        mkt.offers = offers
        mkt.settle()
        

    def testIntersect8(self):
        # should clear at $30
        demand = [(0.0, 50.0), (100.0, 10.0)]
        supply = [(50.0, 10.0), (50.0, Offer.HUGE)]
        dPoly = PolyLineFactory.fromTupples(demand)
        sPoly = PolyLineFactory.fromTupples(supply)
        mkt= Market()
        mkt.name = "testMarket"
        mkt.commodity = "commodity"
        offers = []
        offers.append(Offer(Offer.BUY, "commodity", dPoly))
        offers.append(Offer(Offer.SELL, "commodity", sPoly))
        mkt.offers = offers
        mkt.settle()
        
        
    def testIntersec9(self):
        # should clear around 72.56
        demand = [[8050.697191352069, 72.72727272727272], [8079.786644062395, 71.81818181818181], [8108.927653866827, 70.9090909090909], [8138.12038794295, 70.0], [8167.365013468356, 69.0909090909091], [8196.661697620633, 68.18181818181819], [8226.010607577362, 67.27272727272728], [8255.411910516139, 66.36363636363636], [8284.86577361454, 65.45454545454545], [8314.372364050156, 64.54545454545455], [8343.93184900058, 63.63636363636363], [8373.544395643388, 62.72727272727273], [8403.210171156177, 61.81818181818181], [8432.929342716527, 60.90909090909091], [8462.702077502028, 60.0], [8492.528542690266, 59.090909090909086], [8522.408905458833, 58.18181818181818], [8552.343332985307, 57.27272727272727], [8582.331992447276, 56.36363636363636], [8612.375051022334, 55.45454545454545], [8642.47267588806, 54.54545454545455], [8672.625034222048, 53.63636363636363], [8702.832293201878, 52.72727272727273], [8733.094620005144, 51.81818181818182], [8763.41218180943, 50.90909090909091], [8793.785145792313, 50.0], [8824.2136791314, 49.090909090909086], [8854.697949004261, 48.18181818181818], [8885.238122588495, 47.27272727272727], [8915.834367061669, 46.36363636363636], [8946.486849601399, 45.45454545454545], [8977.195737385246, 44.54545454545455], [9007.961197590812, 43.63636363636363], [9038.783397395673, 42.72727272727273], [9069.662503977424, 41.81818181818181], [9100.598684513658, 40.90909090909091], [9131.592106181946, 40.0], [9162.642936159878, 39.09090909090909], [9193.751341625055, 38.18181818181818], [9224.917489755055, 37.27272727272727], [9256.14154772746, 36.36363636363636], [9287.42368271986, 35.45454545454545], [9318.764061909842, 34.54545454545455], [9350.162852474992, 33.63636363636364], [9381.62022159291, 32.72727272727273], [9413.136336441159, 31.818181818181817], [9444.711364197341, 30.90909090909091], [9476.34547203904, 30.0], [9508.038827143851, 29.09090909090909], [9539.791596689345, 28.18181818181818], [9571.60394785312, 27.272727272727273], [9603.476047812757, 26.363636363636363], [9635.40806374585, 25.454545454545453], [9667.40016282998, 24.545454545454547], [9699.452512242733, 23.636363636363637], [9731.565279161696, 22.727272727272727], [9763.738630764465, 21.81818181818182], [9795.972734228615, 20.909090909090907], [9828.267756731739, 20.0], [9860.623865451424, 19.09090909090909], [9893.041227565254, 18.18181818181818], [9925.520010250824, 17.272727272727273], [9958.060380685703, 16.363636363636363], [9990.662506047498, 15.454545454545453], [10023.326553513783, 14.545454545454545], [10056.052690262151, 13.636363636363637], [10088.841083470184, 12.727272727272727], [10121.691900315474, 11.818181818181818], [10154.605307975608, 10.909090909090908], [10187.581473628168, 10.0]]
        supply = [[8056.0, 55.0], [8056.0, 1000000.0]]
        dPoly = PolyLineFactory.fromTupples(demand)
        sPoly = PolyLineFactory.fromTupples(supply)
        print PolyLine.intersection(dPoly.tuppleize(), sPoly.tuppleize())  
        mkt= Market()
        mkt.name = "testMarket"
        mkt.commodity = "commodity"
        offers = []
        offers.append(Offer(Offer.BUY, "commodity", dPoly))
        offers.append(Offer(Offer.SELL, "commodity", sPoly))
        mkt.offers = offers
        mkt.settle()
    
        
        
    
    def testNonePolyLineCombine(self):
        l1 = PolyLine()
        l1.add(Point(None, None))
        l2 = PolyLine()
        l2.add(Point(None, None))
        l3 = PolyLineFactory.combine([l1, l2], 100)
        print l3.points[0].x
        
        
    def testNonePolyLineCombine2(self):
        l1 = PolyLine()
        l1.add(Point(None, None))
        l1.add(Point(1, None))
        l1.add(Point(None, 1))
        l2 = PolyLine()
        l2.add(Point(None, None))
        l3 = PolyLineFactory.combine([l1, l2], 100)
        print l3.points[0].y
        
        
    def testNonePolyLineCombine3(self):
        l1 = PolyLine()
        l1.add(Point(None, None))
        l1.add(Point(1, None))
        l1.add(Point(None, 1))
        l2 = PolyLine()
        l2.add(Point(None, None))
        l2.add(Point(1, 1))
        l3 = PolyLineFactory.combine([l1, l2], 100)
        print l3.points[0].y
        
        
    def testNonePolyLineCombine4(self):
        l1 = PolyLine()
        l1.add(Point(None, None))
        l1.add(Point(1, None))
        l1.add(Point(None, 1))
        l1.add(Point(1, 1))
        l2 = PolyLine()
        l2.add(Point(None, None))
        l2.add(Point(1, None))
        l2.add(Point(None, 1))
        l2.add(Point(2, 2))
        l3 = PolyLineFactory.combine([l1, l2], 5)
        print l3.points[0].y
        
        
    def testNoneLine(self):
        l1 = PolyLine()
        l1.add(Point(None, None))
        print l1.x(2)
        print l1.x(None)
        
        
    def testForceSettle(self):
        # should clear at $30
        demand = [(0.0, 50.0), (100.0, 30.0)]
        supply = [(0.0, 0.0), (100.0, 20.0)]
        dPoly = PolyLineFactory.fromTupples(demand)
        sPoly = PolyLineFactory.fromTupples(supply)
        mkt= Market()
        mkt.name = "testMarket"
        mkt.commodity = "commodity"
        offers = []
        offers.append(Offer(Offer.BUY, "commodity", dPoly))
        offers.append(Offer(Offer.SELL, "commodity", sPoly))
        mkt.offers = offers
        mkt.settle()

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()